## Scrumify


At this point we have our GKE cluster up and running along with our database, so we only need to deploy our application scrumify.  
The major steps we want to perform are: 
1.     Compile the application code
2.     Package the application
3.     Process SonarQube on the code
4.     Create a docker image from the Dockerfile (Dockerfile specifies how your code gets packaged into a container)
5.     Push the docker image to a image registry
6.     Create deployment kubernetes object containing the scrumify application
7.     Create the respective kubernetes service
8.     Remove the docker image from the image registry



Assuming that we have Jenkins and SonarQube VM machine running, we will create an CI/CD pipeline by including all these instruction on a Jenkins file.  
The real advantage we got from using a pipeline is that, when we improve the code of scrumify and push a new version to the repository, Jenkins will handle with all compilation, quality and deployment stages of scrumify.  
On the other hand, if anything wrong happens during the course of the pipeline, Jenkins will tell us which stage the pipeline failed and respective reason, which is an important information that can be used for debugging purposes. 

### Associate Jenkins to Git repo

Create a new repository on Gitlab https://gitlab.com/ with your code. You can use Accenture credentials do create an account.

On the Jenkins virtual machine, open the blue ocean interface and create a new pipeline, select the repository type (i.e. git) and insert your repository url. After that you need to provide a credential to that repository (username and password). This process will create a multibranch pipeline connected to the given repository. Now this multibranch pipeline can run the Jenkinsfile of any branch independently from the other branches (i.e. a push in one branch will only trigger the execution of the pipeline associated with that branch).

In order to run the Jenkinsfile pipeline in a specific branch on every push to that branch, you need to setup a webhook in your repository that will send a notification to the Jenkins machine on every push. In gitlab, that is done in Settings -> Integrations -> Jenkins CI. In this page you can specify when you want the repository to send the notification and where to send it (your Jenkins machine information).

### Jenkins Pipeline

Open Jenkinsfile and fill the fields `environment` and `stages`.
Note: Continue to use the readmes from the original repo.


#### Environment

Before starting building the pipeline, we have to create some variables. These variables value either will be used here on the pipeline context or on the application properties/pom.xml of the application.



`APP_VERSION`
The application version on pom.xml.
This value can be obtain issuing: `mvn help:evaluate -Dexpression=project.version -q -DforceStdout`

`MAVEN_PROFILE`
This is the profile to use when running or packing scrumify.
For now only consider the following the profiles that you can find on pom.xml: "dev" and "qa".
You don't need to hardcode this field. You can retrieve its value by capturing the first word of the branch name, using the "$GIT_BRANCH" variable.  
e.g.: getting the 'qa' out of branch name "qa-mycluster".

`CLUSTER_NAME`
This is the GCP cluster name.
You can retrieve its value by capturing the second word or token of the branch name.

`DEV_DB_URL`; 
`DEV_DB_USER`; 
`DEV_DB_PASS`; 
`QA_DB_URL`; 
`QA_DB_USER`; 
`QA_DB_PASS`

These variables correspond to database urls, users and passwords and they will be inputted in the pom.xml of the application. Since they correspond to sensitive information, you can use jenkins credentials to store this type of information. Thus, we prevent it from being hardcoded and exposed in this project taking the chance of somebody not intended to get it.


Steps to create a new credential on Jenkins server:
1. Go to __Manage Jenkins__
2. Click on __Manage Credentials__
3. Go to __Jenkins store__
4. Click on __Global credentials (unrestricted)__
5. Click on __Add Credentials__

To define the 6 credentials the following values can be used (all of type "Secret Text"):

* ID: `dev-db-url`, Value: `jdbc:postgresql://postgres.${env.BRANCH_NAME}.svc.cluster.local:5432/postgresdb?currentSchema=${spring.database.schema}`, Description: `Database URL for dev profile`
* ID: `dev-db-user`, Value: `postgresadmin`, Description: `User of database in dev profile`
* ID: `dev-db-pass`, Value: `*****`, Description: `Password of database in dev profile`
* ID: `qa-db-url`, Value: `jdbc:postgresql://postgres.${env.BRANCH_NAME}.svc.cluster.local:5432/postgresdb?currentSchema=${spring.database.schema}`, Description: `Database URL for qa profile`
* ID: `qa-db-user`, Value: `postgresadmin`, Description: `User of database in qa profile`
* ID: `qa-db-pass`, Value: `*****`, Description: `Password of database in qa profile`


For the case of `DEV_DB_URL`, after you created the credential 'dev-db-url' in Jenkins, here on the pipeline context you can set this variable by doing: 
```sh
DEV_DB_URL = credentials('dev-db-url')
```

#### Stages

Create the following stages using the name provided and respecting the instructions for each stage:

##### Stage: Build

Add these 2 commands to your list of steps.

- Compilation: `mvn compile -DskipTests`
- Packaging: `mvn clean package -P${MAVEN_PROFILE} -DskipTests -Dmaven.install.skip=true`

The end result of this stage, should look like this:
```sh
stage('Build') {
    steps{
        sh'<command #1>'
        sh'<command #2>'
    }
}
```

##### Stage: SonarQube

###### Create the VM to run SonarQube

In case you haven't already created, [refer here](sonarqube.md) for documentation on how to create and configure the sonarqube machine.

###### Configure Jenkins to use SonarQube
You can add this snippet into your Jenkinsfile.  
It evaluates the application in terms of test coverage, duplicated lines, maintainability, reliability and security ratings and security hotsposts.

```sh
stage('SonarQube') {
    stages {
        stage('SonarQube analysis'){
        steps {
            withSonarQubeEnv('sonarqube'){
            sh "mvn sonar:sonar"
            }
        }
    }
    stage("SonarQube Quality Gate"){
    steps {
        script {
        timeout(time: 5, unit: 'MINUTES') {
            def qg = waitForQualityGate()
            if (qg.status != 'OK') {
            error "Pipeline aborted due to quality gate failure: ${qg.status}"
            }
        }
        }
    }
    }
    }
}
```

##### Stage: Docker Build

-  Register gcloud as a Docker credential helper: `gcloud auth configure-docker`  
-  Docker build:
    ```sh
    Tag: --tag=scrumify-${BRANCH_NAME}:v${APP_VERSION}
    Path: ./
    ```
    Docker build documentation: 
    https://docs.docker.com/engine/reference/commandline/build/



##### Stage: Publish Image

- Tag docker image
    ```sh
    source: scrumify-${BRANCH_NAME}:v${APP_VERSION}     
    target: eu.gcr.io/${GCP_PROJECT_ID}/scrumify-${BRANCH_NAME}:v${APP_VERSION}   
    ```
    Docker image tag documentation: https://docs.docker.com/engine/reference/commandline/image_tag/

- Push image to registry:
`docker push eu.gcr.io/${GCP_PROJECT_ID}/scrumify-${BRANCH_NAME}:v${APP_VERSION}`

- Use the service account jenkins-sa@acn-gcp-cloudtraining.iam.gserviceaccount.com that exists in the GCP project to have the correct permissions to perform the docker push.


##### Stage: Deployment

Before start doing modifications into the cluster, this is issuing ctl commands, it's a good practice to make sure which cluster are we talking to.  
Place this command on the beginning of this stage: `gcloud container clusters get-credentials ${CLUSTER_NAME} --region <project_zone> --project ${GCP_PROJECT_ID}`

Brief explanation about the files involved in this stage:

1. deployment.yaml
In this file, you will define the application which will be deployed in the Kubernetes
also in this file, you canto define how to verify if the application is up (livenessProbe), ready(readinessProbe) and also some annotations as to how Prometheus will be able to scrape the application and any other configuration that the application will need, including the selector which will be necessary by the service.  
    documentation: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/

2. service.yaml 
This file will define the access point for the deployment based on the selector.  
obs: since we are using istio as ingress point we are setting which is the version selector in the DestinationRule label.  
    documentation: https://kubernetes.io/docs/concepts/services-networking/service/


```sh
1. File kubernetes/namespace.yaml
    Replace "##Branch##" by ${BRANCH_NAME}
2. File: kubernetes/deployment.yaml
    Replace "##Project##" by ${GCP_PROJECT_ID}
            "##Revision##" by ${APP_VERSION}
            "##Branch##" by ${BRANCH_NAME}
3. File: kubernetes/service.yaml
    Replace "##Revision##" by ${APP_VERSION}
            "##Branch##" by ${BRANCH_NAME}
    

4. Allow for side car injection in your namespaces by running `kubectl label namespace <namespace> istio-injection=enabled istio.io/rev-` for each of the namespaces you have

5. Apply the kubernetes/namespace.yaml

6. Apply kubernetes/deployment.yaml

7. Apply kubernetes/service.yaml, only on the first Jenkins execution

8. Expose the front end by applying the kubernetes/frontend-ingress.yaml. You can access the application through the ingress IP

9. Delete the previous kubernetes deployment, after the first Jenkins execution.

10. Remove docker image from docker

11. Remove docker image from the registry

```
The moment you feel the pipeline is working, you may increment the scumify application version on pom.xml everytime you want to trigger a pipeline execution:
For instance:

    1º Jenkins Execution:
    Start by **pushing a v1.5.43**(default version number of scrumify), which will trigger a jenkins pipeline to migrate a scrumify application v1.5.43.
    Make sure the respective kubernetes deployment exists but not the respective docker image from docker and registry.

    2º Jenkins Execution:
    After the first pipeline is over, **push a v1.5.44** and check if in fact it was created a v1.5.44 deployment.
    Also check the v1.5.43 deployment no longer exists as well as the docker and registry image of v1.5.44.

<details><summary>GKE-Auth-Plugin Error</summary>    
Note: In case you encounter the error related to gke-gcloud-auth-plugin when running kubectl commands, please check this link: https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke

You will first need to install the plugin in the Jenkins virtual machine. For example, if it is a debian VM you will need to run:
    
    sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin

You will then need to add an export command before running the container clusters get-credentials command in the pipeline. They must be inside the same shell script:
    
    sh """export USE_GKE_GCLOUD_AUTH_PLUGIN=True
    gcloud container clusters get-credentials ${CLUSTER_NAME} --region ${REGION} --project ${GCP_PROJECT_ID}"""
</details>

### Execute Jenkins Pipeline & Interact with Application

After creating the whole Jenkins pipeline, you can trigger it to place on top of the cluster a scrumify application with the version previously specified.
You can watch on Jenkins > BlueOcean the execution of your pipeline. If the pipeline had succeded, try to interact with the scrumify using a web browser.

    1. Trigger Jenkins pipeline
    2. Take only the ip address of the istio-ingressgateway ( External Load Balancer)
    3. Take the url from your istio-config.yaml: scrumify.##Branch##.gke-demo.at.pt
    4. Add these values in windows host file (in order to save this file you need admin priviledges)
    4. Access to the last url on the browser
    5. Interact with scrumify using the following credentials: developer3, 9999

