## Before you start

Create a new VM in the same way you created the Jenkins VM. 

Ensure the required ports are open and can be reached by Jenkins VM, and that the machine has an external IP available.

### Allow traffic into the VM
Allow traffic to pass into the VM from the outside by opening port 9000 and allowing HTTP and HTTPS traffic.

1 On the configurations of the VM, enable the following parameters:
    
    * *Allow HTTP traffic*
    * *Allow HTTPS traffic*
    
2. Allow traffic in port 9000 on the firewall
    
    * On GCP project, go to the __VPC Network__ section
    * On the left menu choose __Firewall__
    * Click on __Create firewall rule__
    
    * Example of configuration of the rule:
        * Name: `sonarqube-port-9000`
        * Target tags: `sonarqube-port-9000`
        * Source IP ranges: `0.0.0.0/0`
        * Enable *Specified protocols and ports*
        * Enable *tcp* and write `9000` on it
    
3. After the rule creation, edit the Jenkins VM to add this new tag in the *network tags*.

4. Allow also traffic on port 22 to be able to SSH into the VM. You can add this port in the previous rule.

5. Allow egress traffic into the VM by adding the already created "allow-egress" tag in the *network tags*


## Configurations inside SonarQube VM

Access the SonarQube VM by __SSH__ and a new window with a bash terminal will be prompted.

### Install Docker and Docker Compose

These commands might change depending on the Linux distribution. Please check https://docs.docker.com/engine/install/ for more information and the correct steps for each distribution.

* **Add the docker repo**
    * `sudo apt-get update`
    * `sudo apt-get install ca-certificates curl gnupg lsb-release` (allows apt to install packages over HTTPS)
    * `sudo mkdir -p /etc/apt/keyrings` (Adding docker GPG key)
    * `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg`
    * `echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`

* **Install the docker engine and docker compose**
    * `sudo apt-get update`
    * `sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin`

* **Check if docker is running**
    * `sudo systemctl status docker`

* **Check if docker compose is installed**
    * `docker compose version`
    
    *  In case of difficulties, the following URL could be useful:
        - https://docs.docker.com/compose/install/linux/#install-using-the-repository

### Install SonarQube

To install SonarQube we will use a `docker-compose.yaml` file, justifying the installation of Docker and Docker Compose.

* Create a new folder and access it.
    * `mkdir sonarqube`
    * `cd sonarqube`
* Create a `docker-compose.yaml` file.
    * `nano docker-compose.yaml`
* Insert the code from the file docker-compose.yml into it
* For machine memory issues, the following command may need to be executed.
    * `sudo sysctl -w vm.max_map_count=262144`
* Execute Docker Compose
    * `sudo docker compose up -d`
    * Other useful commands:
        * `sudo docker compose logs` (to verify the logs)
        * `sudo docker ps`
        * `sudo docker ps -a`

## Configurations on SonarQube server

Open the SonarQube server in your browser (`http://<sonarqube-vm-ip-address>:9000`)

* The default login is `admin` and the default password is also `admin`.
* After the authentication step, it is a good practice to change the administrator password.
* You can now create a new SonarQube project, for example:
    * Project key: `scrumify`
    * Display name: `scrumify`
    * Generate a token and store the token somewhere  (Token name: `scrumify`)

## Use SonarQube with Jenkins pipeline

### Configurations

* Install *SonarQube Scanner* plugin in your Jenkins server.
* On Jenkins server, go to __Configure System__ and configure __SonarQube servers__.
    * Click to add server
    * Name: `sonarqube`
    * Server URL: `http://<sonarqube-vm-ip-address>:9000`
    * Enable the parameter *Enable injection of SonarQube server configuration as build environment variables*
    * Configure the access token with a Jenkins *Secret Text* (the access token is the token generated on SonarQube)
* On Jenkins server, go to __Global Tool Configuration__ and configure __SonarQube Scanner__.
    * Add scaner
    * Name: `sonarqube`
    * Enable the parameter *Install automatically*

### Use SonarQube Quality Gate on Jenkins pipeline

* To use the Quality Gate on Jenkins pipeline and therefore use the method `waitForQualityGate()` on `Jenkinsfile` it is necessary to configure a webhook on SonarQube that points to `http://<jenkins-vm-ip-address>:8080/sonarqube-webhook/`.
    * On the SonarQube project go to __Project Settings__ and click on __Webhooks__
    * Create a webhook, for example:
        * Name: `Jenkins Webhook`
        * URL: `http://<jenkins-vm-ip-address>:8080/sonarqube-webhook/`
        * The secret is not necessary
        