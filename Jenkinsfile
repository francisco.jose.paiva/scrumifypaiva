pipeline {
    agent any
    environment {
        def APP_VERSION = sh(script: 'mvn help:evaluate -Dexpression=project.version -q -DforceStdout', returnStdout: true).trim()
        def BRANCH_NAME = "${GIT_BRANCH}"
        def GCP_PROJECT_ID = 'acn-gcp-cloudtraining'
        def dockerImage = "scrumify-${BRANCH_NAME}:v${APP_VERSION}"

        //###############################################
        def CLUSTER_NAME = 'my-gke-cluster-paiva'

        def APPLIED_LABEL = 'applied=true'




        def serviceFilePath = 'kubernetes/service.yaml'

    }

    stages {

        stage('Build') {
            steps {
                script {
                    def MAVEN_PROFILE
                    if (BRANCH_NAME.contains('-')) {
                        MAVEN_PROFILE = sh(script: "echo ${BRANCH_NAME} | cut -d'-' -f1", returnStdout: true).trim()
                    } else {
                        echo "Debug: BRANCH_NAME does not contain a hyphen."
                        MAVEN_PROFILE = BRANCH_NAME
                    }
                    echo "MAVEN_PROFILE: ${MAVEN_PROFILE}"
                    sh 'mvn compile -DskipTests'
                    sh "mvn clean package -P${MAVEN_PROFILE} -DskipTests -Dmaven.install.skip=true"
                }
            }
        }

        stage('SonarQube') {
            steps {
                withSonarQubeEnv('sonarqube') {
                    script {
                        def sonarqubeUrl = "http://35.241.198.164:8080" // Update the port here
                        sh "mvn sonar:sonar -X -Dsonar.host.url=${sonarqubeUrl}"
                    }
                }
            }
        }

/*
        stage('SonarQube') {
            steps {
                withSonarQubeEnv('SonarQube-Server') {
                    script {
                        def sonarqubeUrl = "http://35.241.198.164:8080" // Update the port here
                        def scannerHome = tool 'sonar-scanner'
                        withEnv(["PATH+SCANNER=${scannerHome}/bin"]) {
                            sh "mvn sonar:sonar -X -Dsonar.host.url=${sonarqubeUrl}"
                        }
                    }
                }
            }
        }
    */
/*
        stage('SonarQube') {
            stages {
                stage('SonarQube analysis'){
                steps {
                    withSonarQubeEnv('sonarqube'){
                    sh "mvn sonar:sonar"
                    }
                }
            }
*/
        stage('SonarQube Quality Gate') {
            steps {
                script {
                    timeout(time: 5, unit: 'MINUTES') {
                        def qg = waitForQualityGate()
                        if (qg.status != 'OK') {
                            error "Pipeline aborted due to quality gate failure: ${qg.status}"
                        }
                    }
                }
            }
        }


        stage('Docker Build') {
            steps {
                script {
                    //def dockerBuildCmd = "docker build -t ${dockerImage} -f ${dockerfilePath}/Dockerfile ."

                    // Run Docker build command
                    //sh dockerBuildCmd
                    sh "docker build -t ${dockerImage} ."

                    // Check if Docker build was successful
                    if (sh(script: "echo \$?", returnStatus: true) != 0) {
                        error "Docker build failed."
                    }
                }
            }
        }

        



        stage('Push Docker Image') {
            steps {
                script {
                    sh "docker tag scrumify-${BRANCH_NAME}:v${APP_VERSION} eu.gcr.io/${GCP_PROJECT_ID}/scrumify-${BRANCH_NAME}:v${APP_VERSION}"
                   
                    def GCLOUD_CREDENTIALS_ID = "ServiceAccount"
 
                    withCredentials([file(credentialsId: GCLOUD_CREDENTIALS_ID, variable: 'GCLOUD_KEY')]) {
                        sh "gcloud auth activate-service-account --key-file=${GCLOUD_KEY}"
                    }
 
                    sh "docker push eu.gcr.io/${GCP_PROJECT_ID}/scrumify-${BRANCH_NAME}:v${APP_VERSION}"
                }
            }
        }

        stage('Deployment') {
            steps {
                script {
                    sh "gcloud container clusters get-credentials ${CLUSTER_NAME} --region europe-west1-b --project ${GCP_PROJECT_ID}"
                    
                   
                    sh'''
                        cd kubernetes
                        cat namespace.yaml
                    '''

                    def NAMESPACE_FILE_CONTENT = sh(script: "cd kubernetes && cat namespace.yaml", returnStdout: true).trim()
                    echo "namespace.yaml (before): ${NAMESPACE_FILE_CONTENT}"

                    sh "sed -i 's/##Branch##/${BRANCH_NAME}/gi' kubernetes/namespace.yaml"
            
                    NAMESPACE_FILE_CONTENT = sh(script: "cd kubernetes && cat namespace.yaml", returnStdout: true).trim()
                    echo "namespace.yaml (after): ${NAMESPACE_FILE_CONTENT}"

                    //########################################################
                    // file DEPLOYMENT
                    // Project --> GCP_PROJECT_ID ----------------------------
                    def DEPLOYMENT_FILE_CONTENT = sh(script: "cd kubernetes && cat deployment.yaml", returnStdout: true).trim()
                    echo "deployment.yaml (before Project): ${DEPLOYMENT_FILE_CONTENT}"

                    sh "sed -i 's/##Project##/${GCP_PROJECT_ID}/gi' kubernetes/deployment.yaml"
            
                    DEPLOYMENT_FILE_CONTENT = sh(script: "cd kubernetes && cat deployment.yaml", returnStdout: true).trim()
                    echo "deployment.yaml (after project): ${DEPLOYMENT_FILE_CONTENT}"
                    
                    //#####################################################
                    // Revision --> APP_VERSION ---------------------------
                    def DEPLOYMENT_FILE_CONTENT_R = sh(script: "cd kubernetes && cat deployment.yaml", returnStdout: true).trim()
                    echo "deployment.yaml (before Revision): ${DEPLOYMENT_FILE_CONTENT_R}"

                    sh "sed -i 's/##Revision##/${APP_VERSION}/gi' kubernetes/deployment.yaml"
            
                    DEPLOYMENT_FILE_CONTENT_R = sh(script: "cd kubernetes && cat deployment.yaml", returnStdout: true).trim()
                    echo "deployment.yaml (after Revision): ${DEPLOYMENT_FILE_CONTENT_R}"
                    
                    //#####################################################
                    // Branch --> BRANCH_NAME -----------------------------
                    def DEPLOYMENT_FILE_CONTENT_B = sh(script: "cd kubernetes && cat deployment.yaml", returnStdout: true).trim()
                    echo "deployment.yaml (before Revision): ${DEPLOYMENT_FILE_CONTENT_B}"

                    sh "sed -i 's/##Branch##/${BRANCH_NAME}/gi' kubernetes/deployment.yaml"
            
                    DEPLOYMENT_FILE_CONTENT_B = sh(script: "cd kubernetes && cat deployment.yaml", returnStdout: true).trim()
                    echo "deployment.yaml (after Revision): ${DEPLOYMENT_FILE_CONTENT_B}"

                    //###########################################################
                    // file service.yaml
                    //  Revision --> APP_VERSION ---------------------------
                    def DEPLOYMENT_FILE_CONTENT_RR = sh(script: "cd kubernetes && cat service.yaml", returnStdout: true).trim()
                    echo "service.yaml (before RevisionR): ${DEPLOYMENT_FILE_CONTENT_RR}"

                    sh "sed -i 's/##Revision##/${APP_VERSION}/gi' kubernetes/service.yaml"
            
                    DEPLOYMENT_FILE_CONTENT_RR = sh(script: "cd kubernetes && cat service.yaml", returnStdout: true).trim()
                    echo "service.yaml (after RevisionR): ${DEPLOYMENT_FILE_CONTENT_RR}"

                    //#####################################################
                    // Branch --> BRANCH_NAME -----------------------------
                    def DEPLOYMENT_FILE_CONTENT_BB = sh(script: "cd kubernetes && cat service.yaml", returnStdout: true).trim()
                    echo "service.yaml (before BranchB): ${DEPLOYMENT_FILE_CONTENT_BB}"

                    sh "sed -i 's/##Branch##/${BRANCH_NAME}/gi' kubernetes/service.yaml"
            
                    DEPLOYMENT_FILE_CONTENT_BB = sh(script: "cd kubernetes && cat service.yaml", returnStdout: true).trim()
                    echo "service.yaml (after BranchB): ${DEPLOYMENT_FILE_CONTENT_BB}"
                    //##########################################################

                
                    //Ponto 4 
                    // Label the namespace for sidecar injection
                    //sh "kubectl label namespace dev-my-gke-cluster-paiva istio-injection=enabled istio.io/rev-"
                    //sh "kubectl label namespace my-gke-cluster-paiva istio-injection=enabled istio.io/rev=${ISTIO_REVISION}"
                    //kubectl label namespace dev-my-gke-cluster-paiva istio-injection=enabled istio.io/rev=<CHOSEN_ISTIO_REVISION>


                    //Ponto 5
                    def NameSpacePath = 'kubernetes/namespace.yaml'
                    sh "kubectl apply -f ${NameSpacePath}"

                    //Ponto 6
                    def DeploymentPath = 'kubernetes/deployment.yaml'
                    sh "kubectl apply -f ${DeploymentPath}"

                    //Ponto 7
                  
                     // Check if the service with the label is already applied
                    def serviceExists = sh(script: "kubectl get services -l ${APPLIED_LABEL} --ignore-not-found", returnStatus: true) == 0
                    //%%%%%%%%%%%%%%%%%%%%%%
                    //sh "kubectl apply -f ${serviceFilePath}"
                    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    // If the service with the label doesn't exist, apply the service configuration
                    if (!serviceExists) {
                        //def serviceFilePath = 'kubernetes/service.yaml' defenido no inicio
                        sh "kubectl apply -f ${serviceFilePath}"
    

                        echo 'Service configuration applied.'
                    } 
                    else {
                        
                        echo 'Service configuration already applied. Skipping.'
                    }

                    //Ponto 8
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    //Expose the front end by applying the kubernetes/frontend-ingress.yaml. You can access the application through the ingress IP
                    sh "cat kubernetes/frontend-ingress.yaml"
                    sh "kubectl apply -f kubernetes/frontend-ingress.yaml -n ${BRANCH_NAME}"
                    //sh "kubectl get namespaces"
                    sleep 30
                    sh "kubectl get ingress"
                    sh "kubectl get ingress -n ${BRANCH_NAME}"
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/*
                    //##############
                    //ponto 9
                    //Delete the previous kubernetes deployment, after the first Jenkins execution.
                    def DEPLOYMENT_NAME = readYaml(file: 'kubernetes/deployment.yaml').metadata.name
                    sh "kubectl delete deployment ${DEPLOYMENT_NAME} -n ${BRANCH_NAME}"
                    echo 'ja apagou'
*/
                    //%%%%%%%%%%%%%%%%%%%%%%
                    //ponto 10
                    //Remove docker image from docker
                    sh 'docker rmi -f $(docker images -q)'
                    //sh "docker rmi -f scrumify-${BRANCH_NAME}:v${APP_VERSION}"
                    

                    //ponto 11
                    //Remove docker image from the registry
                    sh "gcloud container images delete eu.gcr.io/${GCP_PROJECT_ID}/scrumify-${BRANCH_NAME}:v${APP_VERSION} --force-delete-tags"
                    


                }       
            }

        }
}







    

    post {
        always {
            // Prune Docker system
            script {
                sh 'docker system prune -af'
            }
        }
        success {
            echo 'I succeeded!'
        }
        failure {
            echo 'OOPS, something went wrong :('
        }
    }
}
