package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;

public class TimesheetTotalView {
   
   @Getter
   @Setter
   private Integer totalHours = 0;
   
   @Getter
   @Setter
   private Integer timespent = 0;
   
   @Getter
   @Setter
   private Integer absences = 0;
   
   @Getter
   @Setter
   private Integer compensationDays = 0;
   
   @Getter
   @Setter
   private Integer holidays = 0;
   
   @Getter
   @Setter
   private Integer vacations = 0;
   
   @Getter
   @Setter
   private Integer occupation = 0;
      
   public TimesheetTotalView() {
      super();
   }

}