package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.TypeOfNote;
import pt.com.scrumify.database.entities.UserStory;
import pt.com.scrumify.database.entities.UserStoryNote;

public class UserStoryNoteView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   private UserStory userStory;
   
   @Getter
   @Setter
   private String content;
   
   @Getter
   @Setter
   private TypeOfNote typeOfNote;

   public UserStoryNoteView() {
      super();
   }

   public UserStoryNoteView(UserStoryNote userstoryNote) {
      super();

      this.setId(userstoryNote.getId());     
      this.setTypeOfNote(userstoryNote.getTypeOfNote());
      this.setContent(userstoryNote.getContent());
      this.setUserStory(userstoryNote.getUserStory());
   }

   public UserStoryNote translate() {
      UserStoryNote userStoryNote = new UserStoryNote();

      userStoryNote.setId(id);      
      userStoryNote.setContent(content);
      userStoryNote.setTypeOfNote(typeOfNote);
      userStoryNote.setUserStory(userStory);

      return userStoryNote;
   }
}