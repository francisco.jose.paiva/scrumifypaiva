package pt.com.scrumify.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Epic;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.services.*;
import pt.com.scrumify.entities.ReportView;
import pt.com.scrumify.entities.ResourceReport;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ReportsHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.services.ExportService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class ReportsController {
   @Autowired
   private DatesHelper datesHelper;
   @Autowired
   private ExportService exportService;
   @Autowired
   private ReportsHelper reportsHelper;
   @Autowired
   private AreaService areasService;
   @Autowired
   private ContractService contractsService;
   @Autowired
   private EpicService epicsService;
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private ReportService reportsService;
   @Autowired
   private SubAreaService subAreasService;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private TimeService timesService;
   @Autowired
   private TimesheetService timesheetsService;
   @Autowired
   private YearService yearsService;
   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);

      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_REPORTS)
   public String list(Model model) {

      return ConstantsHelper.VIEW_REPORTS_READ;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_REPORTS_GENERATE)
   public String generate(Model model) {
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MINDATE, datesHelper.getFirstDayOfMinimumYear());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MAXDATE, datesHelper.getLastDayOfMaximumYear());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, areasService.getByResource(ResourcesHelper.getResource()));      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT, new ReportView());
      
      return ConstantsHelper.VIEW_REPORTS_GENERATE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_REPORTS_MME)
   public String importMME(Model model) {
      
      return ConstantsHelper.VIEW_REPORTS_MME;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_REPORTS_UPDATE_AREA)
   public String changeArea(Model model , @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT) @Valid ReportView reportView) {
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT, reportView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, areasService.getByResource(ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, subAreasService.getByAreaAndResource(reportView.getArea(), ResourcesHelper.getResource()));
      
      return ConstantsHelper.VIEW_REPORTS_GENERATE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_REPORTS_UPDATE_SUBAREA)
   public String changeSubArea(Model model , @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT) @Valid ReportView reportView) {
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT, reportView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, areasService.getByResource(ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, subAreasService.getByAreaAndResource(reportView.getArea(), ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, contractsService.getBySubareaAndResource(reportView.getSubArea(), ResourcesHelper.getResource()));
      
      return ConstantsHelper.VIEW_REPORTS_GENERATE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_REPORTS_UPDATE_CONTRACT)
   public String changeContract(Model model , @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT) @Valid ReportView reportView) {
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT, reportView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, areasService.getByResource(ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, subAreasService.getByAreaAndResource(reportView.getArea(), ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, contractsService.getBySubareaAndResource(reportView.getSubArea(), ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, resourcesService.getMembersOfSameTeamAndContract(reportView.getContract()));
      
      return ConstantsHelper.VIEW_REPORTS_GENERATE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_REPORTS_GENERATE)
   public ResponseEntity<byte[]> generate(HttpServletResponse response, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT) @Valid ReportView reportView) throws IOException {
      
      File file = reportsHelper.generateReport(reportView);
      byte[] output = Files.readAllBytes(file.toPath());

      HttpHeaders responseHeaders = new HttpHeaders();
      responseHeaders.set("charset", "utf-8");
      responseHeaders.setContentType(MediaType.valueOf("text/html"));
      responseHeaders.setContentLength(output.length);
      responseHeaders.set("Content-disposition", "attachment; filename=" + file.getName());

      return new ResponseEntity<>(output, responseHeaders, HttpStatus.OK);
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_REPORTS_CREATE + ConstantsHelper.MAPPING_SLASH + ConstantsHelper.MAPPING_PARAMETER_TYPEOFREPORT)
   public String create(Model model, @PathVariable String typeofreport) {
      ReportView report = new ReportView();
      report.setType(typeofreport);
      report.setYear(new Year(DatesHelper.year()));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearsService.getAll());
      
      if (typeofreport.equals(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET)) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MONTHS, timesService.getMonths());
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_FORTNIGHTS, ConstantsHelper.Fortnight.values());
         report.setMonth(LocalDate.now().getMonthValue());
      }
      else {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, areasService.getByResource(ResourcesHelper.getResource()));
      }

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT, report);
      
      if(typeofreport.equals(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCE)){
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMember(ResourcesHelper.getResource()));
         return ConstantsHelper.VIEW_REPORTS_ABSENCES;
      }
      else if(typeofreport.equals(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM)) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, subAreasService.listSubareasByResource(ResourcesHelper.getResource()));
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMember(ResourcesHelper.getResource()));
         return ConstantsHelper.VIEW_REPORTS_WORKITEMS;
      }
      else if (typeofreport.equals(ConstantsHelper.VIEW_ATTRIBUTE_BACKLOG)) {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, subAreasService.listSubareasByResource(ResourcesHelper.getResource()));
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByMember(ResourcesHelper.getResource()));
         
         return ConstantsHelper.VIEW_REPORTS_BACKLOG;
      }

      return ConstantsHelper.VIEW_REPORTS_SEARCH;
   }

   @PostMapping(value = ConstantsHelper.MAPPING_REPORTS + ConstantsHelper.MAPPING_EXPORT)
   public ResponseEntity<InputStreamResource> export(HttpServletResponse response, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT) @Valid ReportView reportView) throws IOException {
      ByteArrayInputStream in = null;
      HttpHeaders headers = new HttpHeaders();
      switch (reportView.getType()) {
         case ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE :
            Map<Resource, List<ResourceReport>> reportbyresource = reportsService.reportByResource(timesheetsService.getSheetsByYear(reportView.getYear()));
            in = exportService.exportReportByResource(reportbyresource);
            headers.add(ConstantsHelper.CONTENTDISPOSITION, "attachment; filename=Report_by_Resource_" + reportView.getYear().getId() + ConstantsHelper.DEFAULT_EXCEL_EXTENSION);
            break;
         case ConstantsHelper.VIEW_ATTRIBUTE_ABSENCE :
            in = exportService.exportReportByAbsences(reportView);
            headers.add(ConstantsHelper.CONTENTDISPOSITION, "attachment; filename=Report_by_Absences" + ConstantsHelper.DEFAULT_EXCEL_EXTENSION);
            break;
         case ConstantsHelper.VIEW_ATTRIBUTE_WORKITEM :
            in = exportService.exportContracts(reportView);
            headers.add(ConstantsHelper.CONTENTDISPOSITION, "attachment; filename=WorkItems" + ConstantsHelper.DEFAULT_EXCEL_EXTENSION);
            break;
         case ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET :
            in = exportService.exportTurnaround(reportView);
            headers.add(ConstantsHelper.CONTENTDISPOSITION, "attachment; filename=TimeSheet_Report" + reportView.getYear().getId() + ConstantsHelper.DEFAULT_EXCEL_EXTENSION);
            break;
         case ConstantsHelper.VIEW_ATTRIBUTE_EPIC :
            List<Epic> epics = epicsService.getToReport(reportView);
            in = exportService.exportReportByEpic(epics);
            headers.add(ConstantsHelper.CONTENTDISPOSITION, "attachment; filename=Report_by_Epic" + reportView.getYear().getId() + ConstantsHelper.DEFAULT_EXCEL_EXTENSION);
            break;
         case ConstantsHelper.VIEW_ATTRIBUTE_BACKLOG:
            in = exportService.exportBacklog(reportView);
            headers.add(ConstantsHelper.CONTENTDISPOSITION, "attachment; filename=Backlog" + ConstantsHelper.DEFAULT_EXCEL_EXTENSION);
            break;
         default :
            break;
      }
      
      if (in != null)
         return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
      else
         return ResponseEntity.badRequest().build();
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_REPORTS_UPDATESEARCHAREA + ConstantsHelper.MAPPING_SLASH + ConstantsHelper.MAPPING_PARAMETER_AREA)
   public String updatesearcharea(Model model, @PathVariable String area) {

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT, new ReportView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsService.getByArea(Integer.parseInt(area)));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, subAreasService.getByAreaAndResource(new Area(Integer.parseInt(area)), ResourcesHelper.getResource()));

      return ConstantsHelper.VIEW_REPORTS_SEARCH_UPDATETEAM;
   }

   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_REPORTS_UPDATESEARCHTEAM + ConstantsHelper.MAPPING_SLASH + ConstantsHelper.MAPPING_PARAMETER_TEAM)
   public String updatesearchteam(Model model, @PathVariable String team) {

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT, new ReportView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, resourcesService.getByTeam(teamsService.getOne(Integer.parseInt(team))));

      return ConstantsHelper.VIEW_REPORTS_SEARCH_UPDATERESOURCE;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_REPORTS_UPDATESEARCHCONTRACT + ConstantsHelper.MAPPING_SLASH + ConstantsHelper.MAPPING_PARAMETER_CONTRACT)
   public String updateSearchContract(Model model, @PathVariable int contract) {

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT, new ReportView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, contractsService.getOne(contract).getTeams());

      return ConstantsHelper.VIEW_REPORTS_WORKITEMS_TEAM;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_REPORTS_READ + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_REPORTS_SEARCH)
   public String searchReport(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT) @Valid ReportView reportView) {

      if (reportView.getType().equals(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCE)) {
         Map<Resource, List<ResourceReport>> reportbyresource = reportsService.reportByResource(timesheetsService.getSheetsByYear(reportView.getYear()));
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORTS, reportbyresource);
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MONTHS, ConstantsHelper.MONTHS);
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SEARCH, true);
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_REPORT, reportView);
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, yearsService.getAll());
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_AREAS, areasService.getByResource(ResourcesHelper.getResource()));
      }
      
      return ConstantsHelper.VIEW_REPORTS_READ;
   }
}