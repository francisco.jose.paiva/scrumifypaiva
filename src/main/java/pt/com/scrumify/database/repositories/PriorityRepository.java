package pt.com.scrumify.database.repositories;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pt.com.scrumify.database.entities.Priority;

import java.util.List;

public interface PriorityRepository extends JpaRepository<Priority, Integer> {
   @Cacheable(value = "priorities")
   @Query(nativeQuery = false,
   value = "SELECT p " +
           "FROM Priority p " +
           "ORDER BY p.sortOrder")
   List<Priority> findAll();
}