package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemAttachment;

import java.util.List;

public interface WorkItemAttachmentRepository extends JpaRepository<WorkItemAttachment, Integer> {
   
   List<WorkItemAttachment> findByWorkItem(WorkItem workItem);
   List<WorkItemAttachment> findByWorkItemId(Integer workItemId);
}