package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Sprint;
import pt.com.scrumify.database.entities.SprintMember;
import pt.com.scrumify.database.entities.SprintMemberPK;

import java.util.List;

public interface SprintMemberRepository extends JpaRepository<SprintMember, SprintMemberPK> {
   @Query(value = "SELECT sm " +
                  "FROM SprintMember sm " +
                  "WHERE sm.pk.sprint = :sprint " +
                  "ORDER BY sm.pk.resource.name")
   List<SprintMember> getBySprint(@Param("sprint") Sprint sprint);

   List<SprintMember> findByPkResource(Resource resource);
   
}