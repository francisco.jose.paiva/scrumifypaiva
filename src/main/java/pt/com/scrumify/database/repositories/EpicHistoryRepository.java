package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.EpicHistory;

import java.util.List;

public interface EpicHistoryRepository extends JpaRepository<EpicHistory, Integer> {

   List<EpicHistory> findByEpicIdOrderByLastUpdateDesc(Integer idepic);
}