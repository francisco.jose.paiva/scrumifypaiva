package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.TypeOfTimesheetApproval;

public interface TypeOfTimesheetBillableRepository extends JpaRepository<TypeOfTimesheetApproval, Integer> {
}