package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Wiki;

import java.util.List;

public interface WikiRepository extends JpaRepository<Wiki, Integer> {
   
   List<Wiki> findDistinctByApplicationSubAreaTeamsMembersPkResourceInAndActiveIsTrue(List<Resource> resource);
   List<Wiki> findByApplicationNullAndActiveIsTrue();
   List<Wiki> findDistinctByApplicationSubAreaTeamsMembersPkResourceIn(List<Resource> resource);
   List<Wiki> findByApplicationNull();
   
}