package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.Role;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Integer> {
   List<Role> findAllByOrderByNameAsc();
}