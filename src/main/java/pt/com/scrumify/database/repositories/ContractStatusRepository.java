package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.ContractStatus;

import java.util.List;


public interface ContractStatusRepository extends JpaRepository<ContractStatus, Integer> {
   List<ContractStatus> findByIdIn(int[] ids);

}