package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Resource;

import java.util.List;

public interface AreaService {
   Area getOne(Integer id);
   Area save(Area area);
   List<Area> listAll();
   List<Area> getByResource (Resource resource);
}