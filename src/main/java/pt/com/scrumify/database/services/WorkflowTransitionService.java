package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.WorkflowTransition;

import java.util.List;

public interface WorkflowTransitionService {
   WorkflowTransition getOne(Integer id);
   WorkflowTransition save(WorkflowTransition transition);
   void delete(WorkflowTransition transition);
   List<WorkflowTransition> getAll();
   
}