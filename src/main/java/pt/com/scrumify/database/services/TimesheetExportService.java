package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.TimesheetExport;

import java.util.List;

public interface TimesheetExportService {
   List<TimesheetExport> findByResourcesYearAndMonth(List<String> resources, Integer year, Integer month);
   List<TimesheetExport> findByResourcesYearMonthAndFortnight(List<String> resources, Integer year, Integer month, Integer fortnight);
}