package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.repositories.TeamRepository;

import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {
   @Autowired
   private TeamRepository repository;
   
   @Override
   public List<Team> getAll() {
      return this.repository.findAll();
   }
   
   @Override
   public Team getOne(Integer id) {
      return this.repository.getOne(id);
   }
   
   @Override
   public Team save(Team team) {
      return this.repository.save(team);
   }
   
   @Override
   public Team archive(Team team) {
      team.setActive(!team.isActive());
      
      return save(team);
   }
   
   @Override
   public List<Team> getByMemberExceptActual(Resource resource, Team team){
      if(team != null){
         return this.repository.getByMemberExceptActual(resource, team);  
      }else{
         return this.repository.getByMember(resource);
      }
   }
   
   @Override
   public List<Team> getByMemberAndInactive(Resource resource) {
      return this.repository.getByMemberAndInactive(resource);
   }
   
   @Override
   public List<Team> getByMemberId(Integer idResource) {
      return this.repository.findByMembersPkResourceIdAndActiveIsTrue(idResource);
   }
   
   @Override
   public List<Team> getByMember(Resource resource) {
      return repository.getByMember(resource);
   }
   
   @Override
   public List<Team> getByMemberAndReviewer(Resource resource) {
      return this.repository.getByMemberAndReviewer(resource);
   }
   
   @Override
   public List<Team> getByMemberAndApprover(Resource resource) {
      return this.repository.getByMemberAndApprover(resource);
   }
   
   @Override
   public List<Team> getByMemberAndReviewerOrApprover(Resource resource) {
      return this.repository.getByMemberAndReviewerOrApprover(resource);
   }   

   @Override
   public List<Team> getByArea (Integer idArea){
      return repository.findBySubAreaAreaIdAndActiveIsTrue(idArea);
   }
   
   @Override
   public boolean isMemberOfContract(Resource resource, Integer contract) {
      return this.repository.isMemberOfContract(resource, contract);
   }
   
   @Override
   public boolean isMemberOfApplication(Resource resource, Integer subArea) {
      return this.repository.isMemberOfApplication(resource, subArea);
   }
   
   @Override
   public boolean isMemberOfWorkItem(Resource resource, Integer workitem) {
      return this.repository.isMemberOfWorkItem(resource, workitem);
   }

   @Override
   public List<Team> getBySubAreasAndResource(List<SubArea> subAreas, Resource resource) {
      return this.repository.getBySubAreasAndResource(subAreas, resource);
   }

   @Override
   public List<Team> getBySubAreasTeamsAndResource(List<SubArea> subAreas, List<Team> teams, Resource resource) {
      return this.repository.getBySubAreasTeamsAndResource(subAreas, teams, resource);
   }
}