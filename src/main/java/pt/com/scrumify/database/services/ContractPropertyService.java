package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.ContractProperty;

public interface ContractPropertyService {

   ContractProperty getOne(int idContract, int idProperty);
   ContractProperty save(ContractProperty property);
   void delete(ContractProperty property);
   ContractProperty getByPropertyNameAndContract(Integer contract, String property);
}