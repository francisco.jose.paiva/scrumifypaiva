package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.ProjectPhase;

import java.util.List;

public interface ProjectPhaseService {
   ProjectPhase getOne(Integer id);
   List<ProjectPhase> getAll();
   List<ProjectPhase> getByTechnicalAssistance(boolean technicalAssistance);
}