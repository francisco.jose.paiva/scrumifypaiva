package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.TypeOfApplication;

import java.util.List;

public interface TypeOfApplicationService {
   TypeOfApplication getOne(Integer id);
   TypeOfApplication save(TypeOfApplication type);
   List<TypeOfApplication> listAll();
}