package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.Resource;

import java.util.List;

public interface ApplicationService {
   Application getOne(Integer id);
   Application save(Application application);
   List<Application> listAll();
   List<Application> listAllByResource(Resource resource);
   List<Application> listAllSortedByName();
}