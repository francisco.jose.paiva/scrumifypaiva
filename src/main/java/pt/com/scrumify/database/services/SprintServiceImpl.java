package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.Sprint;
import pt.com.scrumify.database.entities.SprintMember;
import pt.com.scrumify.database.repositories.SprintMemberRepository;
import pt.com.scrumify.database.repositories.SprintRepository;
import pt.com.scrumify.helpers.ResourcesHelper;

import java.util.List;

@Service
public class SprintServiceImpl implements SprintService {
   @Autowired
   private SprintRepository repository;
   @Autowired
   private SprintMemberRepository membersRepository;

   @Override
   public Sprint getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public Sprint save(Sprint sprint) {
      
      Sprint saved = repository.save(sprint);

      if(sprint.getId() == 0) {
         SprintMember member = new SprintMember(saved, ResourcesHelper.getResource(), "Product Owner");
         membersRepository.save(member);
      }
      
      return saved;
   }
   
   @Override
   public void delete(Sprint sprint) {
      repository.delete(sprint);
   }

   @Override
   public List<Sprint> listAll() {
      return repository.findAll();
   }
}