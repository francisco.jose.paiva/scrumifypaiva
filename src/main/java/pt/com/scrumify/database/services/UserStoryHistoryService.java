package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.UserStoryHistory;

import java.util.List;

public interface UserStoryHistoryService {
	
   UserStoryHistory save(UserStoryHistory userStoryHistory);
   List<UserStoryHistory> getUserStoryHistorybyUserStory(Integer iduserstory);
  
}