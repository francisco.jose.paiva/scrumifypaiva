package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.ScheduleSetting;

import java.util.List;

public interface ScheduleSettingService {
   
   ScheduleSetting getOne(Integer id);
   ScheduleSetting save(ScheduleSetting setting);	
   void delete(ScheduleSetting setting);
   List<ScheduleSetting> getAll();
   
}