package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Sprint;

import java.util.List;

public interface SprintService {
   Sprint getOne(Integer id);
   Sprint save(Sprint area);
   List<Sprint> listAll();
   void delete(Sprint area);
   
}