package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.Priority;
import pt.com.scrumify.database.repositories.PriorityRepository;

import java.util.List;

@Service
public class PriorityServiceImpl implements PriorityService {
   @Autowired
   private PriorityRepository repository;

   @Override
   public Priority getOne(Integer id) {
      return this.repository.getOne(id);
   }

   @Override
   public List<Priority> getAll() {
      return this.repository.findAll();
   }
}