package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Role;

import java.util.List;

public interface RoleService {
   Role getOne(Integer id);
   Role save(Role role);
   List<Role> getAll();
   List<Role> getAllOrderedByName();
}