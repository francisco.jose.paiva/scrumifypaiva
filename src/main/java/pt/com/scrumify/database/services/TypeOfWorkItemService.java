package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.TypeOfWorkItem;

import java.util.List;

public interface TypeOfWorkItemService {
   List<TypeOfWorkItem> getAll();
   List<TypeOfWorkItem> getAvailable();
   TypeOfWorkItem getOne(Integer id);

}
