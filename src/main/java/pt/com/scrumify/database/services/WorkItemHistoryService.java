package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.WorkItemHistory;

import java.util.List;

public interface WorkItemHistoryService {
	
   WorkItemHistory save(WorkItemHistory workItemHistory);
   List<WorkItemHistory> getWorkItemHistoryByWorkItem(Integer idworkitem);
  
}