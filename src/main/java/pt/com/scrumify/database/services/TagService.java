package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Tag;

import java.util.List;

public interface TagService {
   Tag getOne(Integer id);
   Tag save(Tag tag);
   List<Tag> getAll(List<SubArea> subAreas);
   void delete(Integer id);   
}