package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.TeamMember;
import pt.com.scrumify.database.entities.TeamMemberPK;
import pt.com.scrumify.database.repositories.TeamMemberRepository;

import java.util.List;


@Service
public class TeamMemberServiceImpl implements TeamMemberService {
   @Autowired
   private TeamMemberRepository repository;
   @Autowired
   private OccupationService occupationsService;

   @Override
   public void delete(TeamMember teamMember) {
      this.repository.delete(teamMember);
   }

   @Override
   public TeamMember getOne(TeamMemberPK pk) {
      return this.repository.getOne(pk);
   }

   @Override
   public List<TeamMember> getByTeam(Team team) {
      return this.repository.getByTeam(team);
   }
   
   @Override
   public TeamMember save(TeamMember teamMember) {
      teamMember.setOccupation(occupationsService.getOne(teamMember.getOccupation().getId()));
      return this.repository.save(teamMember);
   }
   
   @Override
   public boolean isMemberOfTeam(Resource resource, Integer team) {
      return this.repository.isMemberOfTeam(resource, team);
   }

   @Override
   public List<TeamMember> getByResource(Resource resource) {
      return repository.findByPkResource(resource);
   }
   
   @Override
   public boolean isApproverOrReviewer(Resource resource) {
      return !repository.findByPkResourceAndApproverIsTrueOrReviewerIsTrue(resource).isEmpty();
   }
}