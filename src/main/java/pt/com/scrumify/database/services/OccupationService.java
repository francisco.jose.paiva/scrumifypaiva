package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Occupation;

import java.util.List;

public interface OccupationService {
   Occupation getOne(Integer id);
   List<Occupation> getAll();
   Occupation save(Occupation occupation);
}