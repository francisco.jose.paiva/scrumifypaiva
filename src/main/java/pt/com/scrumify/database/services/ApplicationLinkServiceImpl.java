package pt.com.scrumify.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.ApplicationLink;
import pt.com.scrumify.database.repositories.ApplicationLinkRepository;

import java.util.List;

@Service
public class ApplicationLinkServiceImpl implements ApplicationLinkService {
   @Autowired
   private ApplicationLinkRepository linksRepository;

   @Override
   public void delete(ApplicationLink link) {
      this.linksRepository.delete(link);
   }

   @Override
   public List<ApplicationLink> getByApplicationAndEnvironment(int application, int environment) {
      return this.linksRepository.findByApplicationIdAndEnvironmentId(application, environment);
   }

   @Override
   public ApplicationLink getOne(int link, int application, int environment) {
      return this.linksRepository.findByIdAndApplicationIdAndEnvironmentId(link, application, environment);
   }

   @Override
   public ApplicationLink save(ApplicationLink link) {
      return this.linksRepository.save(link);
   }
}