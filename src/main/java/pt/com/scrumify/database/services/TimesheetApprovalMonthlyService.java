package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.TimesheetApprovalMonthly;

import java.util.List;

public interface TimesheetApprovalMonthlyService {
   List<TimesheetApprovalMonthly> findByContractsAndYear(List<Contract> contracts, int year);
}