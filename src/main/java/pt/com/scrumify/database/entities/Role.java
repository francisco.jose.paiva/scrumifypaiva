package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_ROLES)
public class Role implements Serializable {
   private static final long serialVersionUID = -7582789396912773371L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;

   @Getter
   @Setter
   @ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
   private List<Profile> profiles = new ArrayList<>(0);

   @Getter
   @Setter
   @ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
   private List<Menu> menu = new ArrayList<>(0);
}