package pt.com.scrumify.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_APPLICATION_LINKS)
public class ApplicationLink implements Serializable {
   private static final long serialVersionUID = -5204362865184421535L;
   
   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;

   @Getter
   @Setter
   @Column(name = "name", nullable = false)
   private String name;

   @Getter
   @Setter
   @Column(name = "url", nullable = true)
   private String url;
   
   @Getter
   @Setter
   @Column(name = "username", nullable = false)
   private String username;
   
   @Getter
   @Setter
   @Column(name = "password", nullable = false)
   private String password;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "application", nullable = false)
   private Application application;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "environment", nullable = false)
   private Environment environment;

   public ApplicationLink(Integer id) {
      this.id = id;
   }
   
   public String getApplicationUrl() {
      return this.url.replaceAll("#username", this.username);
   }
}