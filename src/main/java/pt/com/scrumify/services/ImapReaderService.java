package pt.com.scrumify.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.helpers.ConstantsHelper;

import javax.mail.*;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import java.util.Arrays;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ImapReaderService {
   private static final Logger logger = LoggerFactory.getLogger(ImapReaderService.class);

   @Autowired
   ResourceService resourceService;
   @Autowired
   WorkItemAutomatorService workItemAutomator;

   @Value(ConstantsHelper.IMAP_MAIL_HOST)
   private String imapMailHost;

   @Value(ConstantsHelper.IMAP_MAIL_PORT)
   private int imapMailPort;

   @Value(ConstantsHelper.IMAP_MAIL_USERNAME)
   private String imapMailUsername;

   @Value(ConstantsHelper.IMAP_MAIL_CODE)
   private String imapMailPassword;

   public void run() {
      try {
         Session session = Session.getDefaultInstance(new Properties());
         Store store = session.getStore("imaps");
         store.connect(this.imapMailHost, this.imapMailPort, this.imapMailUsername, this.imapMailPassword);

         Folder inbox = store.getFolder("INBOX");
         inbox.open(Folder.READ_WRITE);

         Message[] messages = inbox.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));

         // Sort messages from oldest to recent
         Arrays.sort(messages, (m1, m2) -> {
            try {
               return m1.getSentDate().compareTo(m2.getSentDate());
            } catch (MessagingException e) {
               logger.info("Exception {} found when trying to send email.", e.getMessage());
               return 0;
            }
         });

         for (Message message : messages) {
            String from = this.getMailSender(message);
            Resource resource = resourceService.getByEmail(from);

            if (resource != null) {
               logger.info("Processing mail sent by '{}' ({}).", resource.getName(), resource.getEmail());
               workItemAutomator.run(resource, message.getSubject(), this.getMailBody((MimeMultipart) message.getContent()));
            } else {
               logger.info("Mail sent by '{}' was discarded (non approved resource).", from);
            }
            
            message.setFlag(Flags.Flag.SEEN, true);
         }

         inbox.close();
      } catch (Exception e) {
         logger.info("Exception {} found when trying to send email.", e.getMessage());
      }
   }

   private String getMailSender(Message message) throws MessagingException {
      Address[] from = message.getFrom();

      Pattern pattern = Pattern.compile("<(.+)@(.+)>");
      Matcher matcher = pattern.matcher(from[0].toString());

      if (matcher.find()) {
         return matcher.group(1) + "@" + matcher.group(2);
      }

      return from[0].toString();
   }

   private String getMailBody(MimeMultipart mimeMultipart) throws Exception {
      StringBuilder result = new StringBuilder();
      int partCount = mimeMultipart.getCount();
      for (int i = 0; i < partCount; i++) {
         BodyPart bodyPart = mimeMultipart.getBodyPart(i);
         if (bodyPart.isMimeType("text/plain")) {
            result.append("\n" + bodyPart.getContent());
            break;
         } else if (bodyPart.isMimeType("text/html")) {
            String html = (String) bodyPart.getContent();

            result = new StringBuilder(html);
         } else if (bodyPart.getContent() instanceof MimeMultipart) {
            result.append(getMailBody((MimeMultipart) bodyPart.getContent()));
         }
      }

      return stringToHTMLString(result.toString());
   }

   public String stringToHTMLString(String string) {
      StringBuilder sb = new StringBuilder(string.length());
      // true if last char was blank
      boolean lastWasBlankChar = false;
      int len = string.length();
      char c;

      for (int i = 0; i < len; i++) {
         c = string.charAt(i);
         if (c == ' ') {
            // blank gets extra work,
            // this solves the problem you get if you replace all
            // blanks with &nbsp;, if you do that you loss
            // word breaking
            if (lastWasBlankChar) {
               lastWasBlankChar = false;
               sb.append("&nbsp;");
            } else {
               lastWasBlankChar = true;
               sb.append(' ');
            }
         } else {
            lastWasBlankChar = false;
            //
            // HTML Special Chars
            htmlSpecialChars(c, sb);
         }
      }
      return sb.toString();
   }
   
   private void htmlSpecialChars(char c , StringBuilder sb) {
      if (c == '"')
         sb.append("&quot;");
      else if (c == '&')
         sb.append("&amp;");
      else if (c == '<')
         sb.append("&lt;");
      else if (c == '>')
         sb.append("&gt;");
      else if (c == '\n')
         // Handle Newline
         sb.append("<br/>");
      else {
         int ci = 0xffff & c;
         if (ci < 160)
            // nothing special only 7 Bit
            sb.append(c);
         else {
            // Not 7 Bit use the unicode system
            sb.append("&#");
            sb.append(Integer.toString(ci));
            sb.append(';');
         }
      }
   }
}