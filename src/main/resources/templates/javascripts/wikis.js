$(document).ready(function() {
   var id = $("input[type=hidden][name='id']").val();
   
   initialize();
});
        
function initialize() {
	
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	formValidation();
}

function formSubmission() {   
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS_SAVE}}]];
   var returnurl = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS}}]];
   
   ajaxpost(url, $("#wikis-form").serialize(), "div[id='wikislist']", true, function() {
      initialize();
   }, returnurl);
}

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
    	  name : {
            identifier : 'name',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
}

function createPage(wikiId) {
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS_PAGES_CREATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + wikiId;
	
	ajaxget(url, "div[id='modal']", function() {
		CKEDITOR.replace('content', {customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]]});
        $('#wikipage-form').form({
            on: 'blur',
            inline : true,
            fields: { 
            	title : {
                  identifier : 'title',
                  rules : [{
                     type : 'empty',
                     prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
                  }]
               },
               content : {
   	              identifier : 'content',
   	              rules : [ {
   	            	  type : 'ckeditorvalidator',
   	                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]],
   	                  value: "textarea[id='content']"
   	            } ]
   	           }
            },
            onSuccess : function(event) {
               event.preventDefault();
               var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS_PAGES_SAVE}}]];
               ajaxpost(url, $("#wikipage-form").serialize(), "div[id='wikis-list']", true, function() {
            	   initialize();
               });
            }
       });
    }, true);   
};

function search(text, event){
	
	if( !$("#searchField").val() ) {
		text = null;
	}
	if(event.key === "Enter") {
		jQuery
		.ajax({
			url : [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS_SEARCH} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + text,
			data: { 'searchField' : text},
			success : function(data) {
				jQuery("#wikis-list").html(data);
			}
		});
	}	
}

function getWikipage(id){
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS_PAGES_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	
	
	
	ajaxget(url, "div[id='page-content']", function() {
	      initialize();
	});
	
}

function editWikipage(id){
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS_PAGES_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
	
	ajaxget(url, "div[id='modal']", function() {
		CKEDITOR.replace('content', {customConfig: [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).DEFAULT_CKEDITOR_CONFIG_FILE}}]]});
        $('#wikipage-form').form({
            on: 'blur',
            inline : true,
            fields: { 
            	title : {
                  identifier : 'title',
                  rules : [{
                     type : 'empty',
                     prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
                  }]
               },
               content : {
   	              identifier : 'content',
   	              rules : [ {
   	            	  type : 'ckeditorvalidator',
   	                  prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]],
   	                  value: "textarea[id='content']"
   	            } ]
   	           }
            },
            onSuccess : function(event) {
               event.preventDefault();
               var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS_PAGES_SAVE}}]];
               
               ajaxpost(url, $("#wikipage-form").serialize(), "div[id='wikis-list']", true, function() {
            	   initialize();
               });
            }
       });
    }, true);  
	
}

function deleteWikipage(id){
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS_PAGES_DELETE_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id ;
	
	ajaxdelete(url, "div[id='wikis-list']", function() {
		initialize();
	},false);   
	
}

function deleteWiki(id){
	
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_WIKIS_DELETE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id ;
	
	ajaxdelete(url, "div[id='wikis-list']", function() {
		initialize();
	},false);   
	
}