$(document).ready(function() {
  initialize();
});
	        
function initialize() {
  $('.ui.dropdown').dropdown();
  $('select.dropdown').dropdown();
  $('.ui.mini').popup({
    position: 'top center'
  });	
}

function approve(id) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_REVIEW_APPROVE}}]] + [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
  var redirect = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_REVIEW}}]];
  
  ajaxget(url, '', null, false, redirect);
}

function reject(id) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_REVIEW_REJECT}}]] + [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
  var redirect = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_REVIEW}}]];
  
  ajaxget(url, '', null, false, redirect);
}

function search() {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_REVIEW_SEARCH}}]];
	
  ajaxpost(url, $("#filter-form").serialize(), "div[id='review-table']", false, function() {
    initialize();
  });
}

function preview(timesheet) {
  var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TIMESHEETS_REVIEW_PREVIEW}}]] + [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + timesheet;
  
  ajaxget(url, "div[id='modal']", function () {
    $("#timesheet-form").form({
      onSuccess: function (event) {
        event.preventDefault();
      }
    })
  }, true);
}