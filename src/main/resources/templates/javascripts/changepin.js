$(document).ready(function() {   
   formValidation();
});

function formSubmission() {   
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCES_CHANGEPIN}}]];
   ajaxpost(url, $("#changepin-form").serialize(), "body", false);
}

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
         pin : {
            identifier : 'password',
            rules : [ {
               type : 'integer[1000..9998]',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_RANGE}(0, 9998, 1000)}]]
            }]
          },
           pin_confirmation_match : {
             identifier : 'passwordConfirmation',
             rules : [ {
                type : 'match[password]',
                prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_PIN_DOESNT_MATCH}(#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_COMMON_PIN_CONFIRMATION}}, #{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_COMMON_PIN}})}]]
            }]
          }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
}