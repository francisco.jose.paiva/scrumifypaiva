window.sidebar = {};

sidebar.ready = function() {
   $("#sidebar").sidebar("setting", "dimPage", true);
   $("#sidebar").sidebar("hide");

   $("#sidebar-menu").click(function() {
      $("#sidebar").sidebar("show");
   }).end();
}

$(document).ready(sidebar.ready);

$('.message .close').on('click', function() {
   $(this)
      .closest('.message')
      .transition('fade')
      ;
});

function ajaxget(requesturl, htmlelement, callbackcomplete, showmodal = false, returnurl = null) {
   $.ajax({
      url: requesturl,
      dataType: 'html',
      type: 'GET',
      success: function(data) {
    	 $(htmlelement).html(data);         

         if (showmodal) {
            $(htmlelement)
            	.modal({
            		autofocus: false
            	})
            	.modal('show');
         }
         
         if(returnurl != null){
     	    window.location.replace(returnurl);
         }
         
      },
      error: function(xhr, status) {
         if (xhr.status == 403) {
            $(htmlelement).html('You are not authorized to access this page.');
         }
      },
      complete: function(xhr, status) {
         if (callbackcomplete != null && status == "success") {
            callbackcomplete();
         }
      }
   });
}

function ajaxpost(requesturl, requestdata, htmlelement, hidemodal, callbackcomplete, returnurl = null) {
   $.ajax({
      url: requesturl,
      data: requestdata,
      type : 'POST',
      success: function(data) {
         if (data.indexOf("DOCTYPE") == -1) {
            $(htmlelement).html(data);
         }
         else {
            $('body').html(data);
         }
         
         if (hidemodal) {
            $('.modal').modal('hide');
         }
         
         if (returnurl != null) {
        	window.location.replace(returnurl);
         }
      },
      error: function(xhr, status) {
         if (xhr.status == 403) {
            $(htmlelement).html('You are not authorized to access this page.');
         }
      },
      complete: function(xhr, status) {
         if (callbackcomplete != null && status == "success") {
            callbackcomplete();
         }
      }
   });
}

function ajaxdelete(requesturl, htmlelement, callbackcomplete, showmodal = false) {
   $("div[id='modal-confirmation']").modal({
      closable: false,
      onApprove: function() {
         ajaxget(requesturl, htmlelement, callbackcomplete, showmodal);
      },
      onDeny: function() {}
   })
   .modal('show');
}

function ajaxtab(history, path, ignorefirstload, mockresponse) {
   $('.pointing.menu .item').tab({
      auto: true,
      cache: false,
      cacheType: 'html',
      history: history,
      historyType: 'state',
      ignoreFirstLoad: ignorefirstload,
      path: path,
      apiSettings: {
         loadingDuration: 20,
         mockResponse: function(settings) {
            mockresponse(settings);
         }
      }
   });
}

function calendar(control, type, localizedtext, min, max, endDate=null) {
   var mindate = new Date(min);
   var maxdate = new Date(max);
   
   var start = '#calendar-' + control;
   var end = null;
   if(endDate!=null){
	   end = '#calendar-' + endDate;
   }
   
   $(start).calendar({
      type : type,
      firstDayOfWeek : 1,
      monthFirst : true,
      today : true,
      minDate : mindate,
      maxDate : maxdate,
      text : localizedtext,
      touchReadonly: true,
      endCalendar: $(end),
      formatter : {
         date : function(date, settings) {
            if (!date)
               return '';
            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();

            return year + '-' + ("00" + month).slice(-2) + '-' + ("00" + day).slice(-2);
         }
      }
   });
   
   if(endDate!=null){
	   $(end).calendar({
	      type : type,
	      firstDayOfWeek : 1,
	      monthFirst : true,
	      today : true,
	      minDate : mindate,
	      maxDate : maxdate,
	      text : localizedtext,
	      touchReadonly: true,
	      startCalendar: $(start),
	      formatter : {
	         date : function(date, settings) {
	            if (!date)
	               return '';
	            var day = date.getDate();
	            var month = date.getMonth() + 1;
	            var year = date.getFullYear();

	            return year + '-' + ("00" + month).slice(-2) + '-' + ("00" + day).slice(-2);
	         }
	      }
	   });
   }
}

function copytoclipboard(text) {
   var textArea = document.createElement("textarea");
   textArea.value = text;
   document.body.appendChild(textArea);
   textArea.select();
   
   document.execCommand("copy");
   textArea.remove();
 }

function getjson(url) {
   $.getJSON(url, function(data) {
      return data;
   });
}

function onblurcalendar(from, to) {
   var field1 = document.getElementById(from);
   var field2 = document.getElementById(to);
   
   if (field1 != null && field1.value != '') {
      field2.value = field1.value.substring(0, 4) + '-' + field1.value.substring(5, 7) + '-' + field1.value.substring(8, 10);
   }
   
   if (field2.value.length > 0) {
      field2.value += " 00:00:00"
   }
}

function tab(history, path) {
   $('.pointing.menu .item').tab({
      history: history,
      path: path
   });
}

$.fn.form.settings.rules.ckeditorvalidator =  function(value, htmlelement) {
	   for(var identifier in CKEDITOR.instances)
	      CKEDITOR.instances[identifier].updateElement();
	   
	   if ($(htmlelement).val() != "")
	      return true;
	};

$.fn.form.settings.rules.ckeditor = function (value, identifier) {
   if (CKEDITOR.instances[identifier].getData() != '')
      return true;
};

$.fn.form.settings.rules.timesheet =  function(value, htmlelement) {
    var result = false;	
    if(value == null || value == ''){
    	return true;
    }
    	
	 $.ajax({
	      url: '/timesheet/submitted/' + value,
	      dataType: 'html',
	      type: 'GET',
	      async: false,
	      success: function(data) {
	    	  result = data  
	      }
	 })
	 
	 return (result == 'true');
};

function filter(startPoint, url, divToReturn){
  
  var $checkboxes = $('#filters input');

  $(':checkbox').each(function() {
    if (this.name == startPoint)
      this.checked = true
  });

  $checkboxes.change(function() {
    var map = [],
      filters = [];
    
    $checkboxes.filter(':checked').each(function() {
      map.push(this.id);
    });

    if (map.length > 0) {
      filters = Object.values(map)
    } else {
      filters = 0
    }
  
    var test = url + '/filter/' + filters;

    ajaxget(test, "div[id='" + divToReturn +  "']", function() {
      $('.ui.dropdown').dropdown();
      $('select.dropdown').dropdown();
      $('.ui.toggle.checkbox').checkbox();
      $('.ui.checkbox').checkbox();
    });
  });
}

function bindMasterAndChildCheckboxes(){
  $('.list .master.checkbox')
  .checkbox({
    // check all children
    onChecked: function() {
      var $childCheckbox  = $(this).closest('.checkbox').siblings('.list').find('.checkbox');
      $childCheckbox.checkbox('check');
    },
    // uncheck all children
    onUnchecked: function() {
      var $childCheckbox  = $(this).closest('.checkbox').siblings('.list').find('.checkbox');
      $childCheckbox.checkbox('uncheck');
    }
  });
  
  $('.list .child.checkbox')
  .checkbox({
    // Fire on load to set parent value
    fireOnInit : true,
    // Change parent state on each child checkbox change
    onChange   : function() {
      var
        $listGroup      = $(this).closest('.list'),
        $parentCheckbox = $listGroup.closest('.item').children('.checkbox'),
        $checkbox       = $listGroup.find('.checkbox'),
        allUnchecked    = true
      ;
      // check to see if all other siblings are checked or unchecked
      $checkbox.each(function() {
        if( $(this).checkbox('is checked') ) {
          allUnchecked = false;
        }
      });
      // set parent checkbox state, but dont trigger its onChange callback
      if(allUnchecked) {
        $parentCheckbox.checkbox('set unchecked');
      } else {
        $parentCheckbox.checkbox('set checked');
      }
    }
  });
}