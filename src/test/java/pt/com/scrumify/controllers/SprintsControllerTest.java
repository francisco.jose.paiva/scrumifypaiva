package pt.com.scrumify.controllers;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.entities.SprintView;
import pt.com.scrumify.helpers.ConstantsHelper;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@DisplayName("Test of sprints controller")
@AutoConfigureMockMvc
@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SprintsControllerTest {
   @Autowired
   private MockMvc mvc;
   
   @Test
   @UnitTest
   @Order(1)
   @MockScrumifyUser(username = "anonymous")  
   public void listByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_SPRINTS))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @UnitTest
   @Order(2)
   @MockScrumifyUser(username = "anonymous")
   public void createByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_SPRINTS_CREATE))
         .andExpect(status().isForbidden());
   }
   
   @Test
   @UnitTest
   @Order(3)
   @MockScrumifyUser(username = "projectdirector")  
   public void listByAllowedUser() throws Exception {
      
      mvc.perform(get(ConstantsHelper.MAPPING_SPRINTS))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_SPRINTS_READ))
         ;
   }
   
   @Test
   @UnitTest
   @Order(4)
   @MockScrumifyUser(username = "projectdirector")  
   public void createByAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_SPRINTS_CREATE))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_SPRINTS_CREATE))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, isA(SprintView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, hasProperty("id", is(0))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SPRINT, hasProperty("name", is(emptyOrNullString()))))
         ;
   }
}